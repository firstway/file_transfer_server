



#include <algorithm>    // std::min

#include "ftc_server.h"

int main(int argc, const char * argv[])
{

    if(argc < 3){
        printf("usage: %s SERVER_PORT ACK_PORT\n", argv[0]);
        return -1;
    };
    int data_port_num = atoi(argv[1]);
    int ack_port_num = atoi(argv[2]);

    FileTrannsferChannelReceiver ftcr;

    //receiver
    Address * recv_addr = new Address("localhost", data_port_num);
    //ReceivingPort *recv_port = new ReceivingPort();
    LossyReceivingPort *recv_port = new LossyReceivingPort(0.5);
    recv_port->setAddress(recv_addr);
    recv_port->init();

    // Sending  ACK Port
    Address * my_addr = new Address("localhost", 3005);
    Address * ack_dst_addr =  new Address("localhost", ack_port_num);
    SendingPort *ack_port = new SendingPort();
    ack_port->setAddress(my_addr);
    ack_port->setRemoteAddress(ack_dst_addr);
    ack_port->init();

    //init Receiver
    ftcr.init_receiver();
    ftcr.set_receiver(recv_port, ack_port);

    ftcr.receive_all();

    return 0;
};

// ##################### receiver
void FileTrannsferChannelReceiver::init_receiver()
{
    init();
    receiving_packet = true;
    seq_man = new SeqManager(_window_size+2);
    seq_man->setStepLen(_default_payload_size);
    to_write_seq = 0;
    first_no_ack_seq = 0;
    last_pm = new PacketMeta(0);
};


void* FileTrannsferChannelReceiver::receiving_packet_thread(void *p)
{
    FileTrannsferChannelReceiver *pftc = (FileTrannsferChannelReceiver*) p;
    Packet *pack;
    while(pftc->receiving_packet){
        Packet *recv_pack = pftc->receiver->receivePacket();
        //copy packet
        pack = new Packet(recv_pack);
        pftc->packet_queue.push(pack);
    }
};


void FileTrannsferChannelReceiver::receive_all()
{
    if(0 != receive_file_name()){
        printf("ERROR: receive_file_name\n");
        return;
    }
    //start the receiving_packet_thread
    pthread_create( &thread_receiving, NULL, &receiving_packet_thread, (void*)this);
    bool isFinish = false;
    while(true){
        int ret = process_packet(isFinish);
        if(isFinish){
            printf("receive_all2 FINISHED\n");
            break;
        };
        send_ack(first_no_ack_seq);
        if(ret <= 0){
            msleep(10);
        };
        if(last_pm->seq > 0){
            //last packet
            if(first_no_ack_seq == last_pm->seq){
                //all packets have been received
                first_no_ack_seq += last_pm->data_len;
            }
            send_ack(first_no_ack_seq);
        }
        write_to_file();
    };
    write_to_file();//rest of buf
    if(_os){
        _os->close();
    }
};

int FileTrannsferChannelReceiver::process_packet(bool &isFinish){
    int pack_count = 0;
    PacketMeta packm;
    while(!packet_queue.empty()){
        pack_count++;
        Packet *recv_pack = packet_queue.front();
        packet_queue.pop();
        parse_packet_header(recv_pack->accessHeader(), &packm);
        //printf("receive_PACK: seq=%lu, data_len=%d, type=[%c]\n", packm.seq, packm.data_len, packm._type);
        if('F'==packm._type){
            printf("receive FINISH packet\n");
            isFinish = true;
            return 0;
        }else if('d'==packm._type || 'L'==packm._type){
            //beyond the range, then  discard
            if( packm.seq >= first_no_ack_seq &&
                packm.seq - to_write_seq + packm.data_len < _pbuf->buf_size()){
                //write to buf
                char *to_write_p = _pbuf->pt_by_seq(packm.seq);
                memcpy(to_write_p, recv_pack->getPayload(), packm.data_len); 
                if('L'==packm._type){
                    *last_pm = packm;
                }else{
                    seq_man->setSeq(packm.seq + packm.data_len);
                    first_no_ack_seq = seq_man->MaxContinSeq();
                }
            }
        }
        delete recv_pack;
    }
    printf("first_no_ack_seq = %lu\n", first_no_ack_seq);
    return pack_count;
}

int FileTrannsferChannelReceiver::send_ack(size_t seq){
        PacketMeta pm(seq);
        Packet ack_pack;
        fill_ack_packet(&ack_pack, &pm);
        ack_sender->sendPacket(&ack_pack);
}

int FileTrannsferChannelReceiver::write_to_file(){
    size_t to_write_size = first_no_ack_seq-to_write_seq;
    if(to_write_size == 0){return -1; };

    char *to_write_p = _pbuf->pt_by_seq(to_write_seq);

    //printf("Write at [%lu], len=[%lu]\n", to_write_seq, to_write_size);
    size_t dist = _pbuf->distance(_pbuf->top_pt(), to_write_p);
    if(0==dist) dist = _pbuf->buf_size();
    to_write_size = std::min(to_write_size, dist );
    printf(">>>>Write at [%lu], len=[%lu]\n", to_write_seq, to_write_size);

    ///print content ot screen TEST
    /*
    char print_buf[100];
    memset(print_buf, 0, 100);
    memcpy(print_buf, to_write_p, to_write_size);
    printf("\n>>[%s]<<\n\n", print_buf);
    */
    
    _os->write(to_write_p, to_write_size);
    to_write_seq += to_write_size;
    return 0;
};


int FileTrannsferChannelReceiver::receive_file_name(){
    PacketMeta *pm = new PacketMeta();
    while(true){
        Packet *pack = receiver->receivePacket();
        parse_packet_header(pack->accessHeader(), pm);
        if(pm->_type= 'i'){
            parse_file_name_packet(_file_name, 100, pack, pm);
            strcpy(_file_name+strlen(_file_name), ".COPY");
            printf("receive_file_name: %s \n", _file_name);
            //send ack
            pack = new Packet();
            pm->convert_ack();
            fill_ack_packet(pack, pm);
            ack_sender->sendPacket(pack);
            delete pack;

            //open output file
            _os = new std::ofstream(_file_name);
            return 0;
        }
    }
    delete pm;
    return -1;
};

