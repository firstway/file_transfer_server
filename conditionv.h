

#ifndef  _conditionv_Header_
#define  _conditionv_Header_
#include <pthread.h>

class CondV
{
public:

    CondV(){
        pthread_mutex_init (&thread_flag_mutex, NULL);
        pthread_cond_init (&thread_flag_cv, NULL);
    }
    ~CondV(){};

    void Wait();
    void Signal();
private:

    pthread_cond_t thread_flag_cv;
    pthread_mutex_t thread_flag_mutex;

};

class MutexV
{
public:
    MutexV(){
        pthread_mutex_init (&mutex, NULL);
        v = 0;
    }
    void Set(int i){
        pthread_mutex_lock( &mutex);
        v = i;
        pthread_mutex_unlock( &mutex);
    };

    int Get(){
        int i = 0;
        pthread_mutex_lock( &mutex);
        i = v;
        pthread_mutex_unlock(&mutex);
        return i;
    };
private:
    int v;
    pthread_mutex_t mutex;
};

#endif

