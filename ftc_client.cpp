


#include <stdio.h>
#include <stdlib.h>

#include <queue>          // std::queue
#include <unistd.h> //sleep
#include <fstream>
#include "ftc.h"
using namespace std;


int main(int argc, const char * argv[])
{

    if(argc < 5){
        printf("NO file\n");
        printf("usage: %s SERVER_IP SERVER_PORT ACK_PORT FILE\n", argv[0]);
        return -1;
    };
    const char* fname = argv[4];
    printf("will send file: [%s]\n",fname);

    int data_port_num = atoi(argv[2]);
    int ack_port_num = atoi(argv[3]);

    std::ifstream ifs;
    ifs.open(fname, std::ifstream::in);

    // SendingPort
    Address * my_addr = new Address("localhost", 3000);
    Address * dst_addr =  new Address("localhost", data_port_num);
    SendingPort *my_port = new SendingPort();
    my_port->setAddress(my_addr);
    my_port->setRemoteAddress(dst_addr);
    my_port->init();

    //ack receiver
    Address * ack_addr = new Address("localhost", ack_port_num);
    //ReceivingPort *ack_port = new ReceivingPort();
    LossyReceivingPort *ack_port = new LossyReceivingPort(0.5);
    ack_port->setAddress(ack_addr);
    ack_port->init();
    //--------------
    FileTrannsferChannelSender  ftcs;
    ftcs.init_sender();
    ftcs.set_sender(my_port, ack_port);
    ftcs.set_file_name(fname);
    ftcs.set_istream(&ifs);
    ftcs.send_all();

    sleep(1);
    printf("FileTrannsferChannelSender Job Done!\n");
    return 0;
}

