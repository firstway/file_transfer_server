
#include "conditionv.h"

void CondV::Wait(){
    pthread_mutex_lock (&thread_flag_mutex);
    pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);
    pthread_mutex_unlock (&thread_flag_mutex);
};

void CondV::Signal(){
     pthread_mutex_lock (&thread_flag_mutex);
     pthread_cond_signal (&thread_flag_cv);
     pthread_mutex_unlock (&thread_flag_mutex);
};

