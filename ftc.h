

#ifndef  _FTC_Header_
#define  _FTC_Header_

#include "common.h"
#include <istream>
#include <ostream>
#include <fstream>
#include <queue>          // std::queue
#include <string.h>
#include <stdio.h>
#include "conditionv.h"
#include "pack_buf.h"
#include "Queue.h"

struct PacketMeta
{
    PacketMeta(){printf("PacketMeta init\n");};
    PacketMeta(size_t tseq, char t='A')//ACK
    {
        version = 1;
        seq = tseq;
        offset = 0;
        data_len = 0;
        _data_pt = NULL;
        _type = t;
    };
    PacketMeta(size_t tseq, char* data, short d_len, char t )
    {
        version = 1;
        seq = tseq;
        offset = 0;
        data_len = d_len;
        _data_pt = data;
        _type = t;
    };
    void convert_ack(){
        version = 1;
        //seq = tseq;
        offset = 0;
        data_len = 0;
        _data_pt = NULL;
        _type = 'A';
    };
    size_t seq;
    short offset;
    short data_len;
    char* _data_pt;
    char version ;
    char _type;
};


class FileTrannsferChannelBase{

public:
    FileTrannsferChannelBase()
    {
        memset(_file_name, 0, 100);
    };
    virtual ~FileTrannsferChannelBase(){};

protected:
    char _file_name[100];
    int err_code ;
    size_t _window_size;
    size_t _default_payload_size;
    PackBuf *_pbuf;

    void init();
    int error_code() const{ return err_code;};
    // fill
    int fill_packet_header(PacketHdr *hdr, const PacketMeta *pm );
    int fill_file_name_packet(Packet *pk, PacketMeta *pm, const char* file_name, int send_seq);
    int fill_data_packet(Packet *pk, PacketMeta *pm);
    int fill_ack_packet(Packet *pk, PacketMeta *pm);
    //parse
    int parse_packet_header(const PacketHdr *hdr, PacketMeta *pm);
    int parse_file_name_packet(char* file_name, int buf_len, Packet *pack, PacketMeta *pm);
};


// Sender
class FileTrannsferChannelSender:public FileTrannsferChannelBase
{
public:
    FileTrannsferChannelSender(){};
    FileTrannsferChannelSender(size_t window_size){
        _window_size = window_size;
    };

    //For sender
    static void* read_file_thread(void *pftc);
    static void* sending_packet_thread(void *pftc);
    static void* reading_ack_thread(void *pftc);
    void init_sender();
    void set_sender(SendingPort *s, ReceivingPort *ack_recv){
        sender = s;
        ack_receiver = ack_recv;
    };
    void set_file_name(const char *file_name){
        strcpy(_file_name, file_name);
    };
    void set_istream(std::istream *is){
        _is = is;
    };
    size_t send_all();

    //other
    void close();
private:
    CondV to_read_condv;
    CondV to_send_condv;
    CondV to_ack_condv;
    int to_read_buf;
    int first_no_ack;
    Queue<PacketMeta*> q_to_send;
    std::queue<int> ack_queue;
    bool read_closed;

    //For sender
    pthread_t thread_sending;
    pthread_t thread_reading;
    pthread_t thread_recv_ack;
    SendingPort   *sender;
    ReceivingPort *ack_receiver;
    std::istream *_is;

    size_t process_ack(bool &isNew);
    int send_file_name();
    int prepare_packet(size_t &curr_seq);
    void finish_transfer(size_t curr_seq);


};


class SeqManager
{
public:
    SeqManager(int num){
        max_conti_seq = 0;
        list_size = num;
        seq_list = new char[list_size];
        memset(seq_list, 0, list_size);
    };
    void setStepLen(size_t s){
        step_len = s;
    };
    int setSeq(size_t seq);
    size_t MaxContinSeq(){return max_conti_seq;};
private:
    SeqManager(){};//can NOT be used
    size_t step_len;
    char *seq_list;
    size_t list_size;
    size_t max_conti_seq;
};


void msleep(int ms);
void print_pm(PacketMeta* pm);
void print_packet(Packet *pk);


#endif

