



#ifndef  _FTC_SERVER_Header_
#define  _FTC_SERVER_Header__

#include "ftc.h"

// Receiver
class FileTrannsferChannelReceiver:public FileTrannsferChannelBase
{
public:
    FileTrannsferChannelReceiver(){};
    ~FileTrannsferChannelReceiver(){};


    static void* writing_file_thread(void *pftc);
    static void* receiving_packet_thread(void *pftc);
    void init_receiver();
    void set_receiver(ReceivingPort *r, SendingPort *s){
        receiver = r;
        ack_sender = s;
    }
    const char* get_file_name() const;
    /*void set_ostream(std::ostream *os){
        _os = os;
    };*/

    void receive_all();


private:
    ReceivingPort *receiver;
    SendingPort   *ack_sender;
    std::ofstream *_os;
    bool receiving_packet;

    SeqManager *seq_man;
    size_t to_write_seq;
    size_t first_no_ack_seq;
    PacketMeta *last_pm;


    pthread_t thread_receiving;

    CondV to_receive_condv;
    CondV to_write_condv;
    std::queue<Packet*> packet_queue;

    int process_packet(bool &isFinish);
    int send_ack(size_t seq);
    int write_to_file();
    //
    int receive_file_name();
};

#endif

