


#ifndef _HEADER_forwarding_
#define _HEADER_forwarding_

#include <sys/socket.h>
#include "Queue.h"

struct thread_variable_send
{
   int scok_fd;
   Queue<char*> *q;
   struct sockaddr *remote_addr;
   socklen_t addrlen;
};
//sendto(fd, my_message, strlen(my_message), 0, (struct sockaddr *)&servaddr, sizeof(servaddr))

struct thread_variable_recv
{
   int scok_fd;
   Queue<char*> *q;
};
//recvlen = recvfrom(fd, buf, BUFSIZE, 0, (struct sockaddr *)&remaddr, &addrlen);

void* port_recv_thread(void *variable_recv);

void* port_send_thread(void *variable_send);






#endif

