
#include "ftc.h"
#include <iostream>
#include <stdio.h>

#include <sys/select.h>
void msleep(int ms)
{
    struct timeval delay;
    delay.tv_sec = 0;
    delay.tv_usec = ms * 1000; // ms
    select(0, NULL, NULL, NULL, &delay);
};

using namespace std;


void FileTrannsferChannelBase::init()
{
    err_code= 0 ;
    _window_size = 32;
    _default_payload_size = 512;
    _pbuf = new PackBuf(_window_size * _default_payload_size);
};


int FileTrannsferChannelBase::fill_packet_header(PacketHdr *hdr, const PacketMeta *pm )
{
    hdr->setHeaderSize(0);//reset length
    hdr->setOctet(pm->version, 0);//version 
	hdr->setIntegerInfo(pm->seq, 1);// seq
    hdr->setShortIntegerInfo(pm->offset, 5);// offset
    hdr->setShortIntegerInfo(pm->data_len, 7);// payload data len 
    hdr->setOctet(pm->_type, 9);//version 
    return 0;
};

int FileTrannsferChannelBase::parse_packet_header(const PacketHdr *hdr,PacketMeta *pm)
{
    pm->version = hdr->getOctet(0);
    pm->seq = hdr->getIntegerInfo(1);
    pm->offset = hdr->getShortIntegerInfo(5);
    pm->data_len = hdr->getShortIntegerInfo(7);
    pm->_type = hdr->getOctet(9);
    return 0;
};

int FileTrannsferChannelBase::fill_file_name_packet(Packet *pk, PacketMeta *pm, const char* file_name, int send_seq){
    int size_read = strlen(file_name);
    pk->fillPayload(size_read, (char*)file_name);
    pm->seq = send_seq;
    pm->offset = 0;
    pm->data_len = size_read;
    pm->_type= 'i';//init file name
    send_seq +=size_read;
    fill_packet_header(pk->accessHeader(), pm );
    return 0;
};

int FileTrannsferChannelBase::parse_file_name_packet(char* file_name, int buf_len,
    Packet *pk, PacketMeta *pm)
{
    memset(file_name, 0, buf_len);
    strncpy(file_name, pk->getPayload(), pk->getPayloadSize());
    parse_packet_header(pk->accessHeader(),pm);
    return 0;
};


int FileTrannsferChannelBase::fill_ack_packet(Packet *pk, PacketMeta *pm)
{
    fill_packet_header(pk->accessHeader(), pm );
    //printf("fill ack pack: seq=%ld\n", pm->seq);
    return 0;
};

int FileTrannsferChannelBase::fill_data_packet(Packet *pk, PacketMeta *pm)
{
    if(pm->data_len > 0){
        pk->fillPayload(pm->data_len, pm->_data_pt);
    }
    fill_packet_header(pk->accessHeader(), pm );
    //printf("fill data pack: seq=%ld\n", pm->seq);
    return pm->data_len;
};
/// =====================================

void FileTrannsferChannelSender::init_sender()
{
    init();
    read_closed = false;

};



size_t FileTrannsferChannelSender::send_all()
{
    //read_file_thread((void*)this);
    pthread_create( &thread_reading, NULL, &read_file_thread, (void*)this);
    pthread_create( &thread_sending, NULL, &sending_packet_thread, (void*)this);
    pthread_create( &thread_recv_ack, NULL, &reading_ack_thread, (void*)this);

    size_t curr_seq = 0;
    size_t no_ack_seq = 0;
    bool new_ack;
    //PacketMeta *pm;
    //fill the first packet of file name
    if(0 != send_file_name() ){
        err_code = -1;
        printf("ERROR : send_file_name\n");
        return 0;
    };

    //prepare pm to send thread
    while(true){
        int m_seq = process_ack(new_ack);
        if(new_ack){
            if(no_ack_seq==m_seq){
                //resend the packet
                PacketMeta *pm = new PacketMeta(no_ack_seq, _pbuf->pt_by_seq(no_ack_seq) , _default_payload_size , 'd');
                q_to_send.push(pm);
                printf("resend the packet, seq=[%lu]\n", no_ack_seq);
                continue;
            }else{
                _pbuf->move_contin_starts(m_seq - no_ack_seq);
                no_ack_seq = m_seq;
                to_read_condv.Signal();
                printf("new_ack:%d\n", m_seq);
            }
        }
        //send new packet
        //printf("prepare_packet....\n");
        int ret = prepare_packet(curr_seq);
        if(2 == ret ){
            printf("have read last packet and papare the FINISH packet\n");
            break;
        }
        //msleep(100);
    }
    finish_transfer(curr_seq);
    return error_code();
};

int FileTrannsferChannelSender::send_file_name()
{
    PacketMeta *pm;
    //fill the first packet and to the queue
    char *p = _file_name + strlen(_file_name);
    while(p > _file_name){
        if('/'==*p) break;
        p--;
    };
    if('/'==*p){p++;};
    pm = new PacketMeta(0, p, strlen(p), 'i');
    q_to_send.push(pm);
    //first file name packet does NOT change Seq
    //MUST wait the first packet ACK
    int count = 1000;
    while( count > 0 ){
        if(!ack_queue.empty()){
            if(ack_queue.front() == 0)
            {
                ack_queue.pop();
                return 0;
            }else return -2;
        }
        msleep(100);
        count--;
    }
    return -1;
};

int FileTrannsferChannelSender::prepare_packet(size_t &curr_seq){
    PacketMeta *pm;
    size_t avail_size = _pbuf->distance( _pbuf->contin_ends_point(), _pbuf->pt_by_seq(curr_seq) );
    if( avail_size > _default_payload_size){
        pm = new PacketMeta(curr_seq, _pbuf->pt_by_seq(curr_seq) , _default_payload_size , 'd');
        curr_seq += pm->data_len;
        q_to_send.push(pm);
        //print_pm(pm);
        return 1;
    }else if(read_closed){
        //last data packet
        if(avail_size > 0){
            pm = new PacketMeta(curr_seq, _pbuf->pt_by_seq(curr_seq) , avail_size, 'L');//LAST packet
            curr_seq += pm->data_len;
            q_to_send.push(pm);
        }
        return 2;
    }
    return 0;
};

size_t FileTrannsferChannelSender::process_ack(bool &isNew){
    //printf(">> process_ack\n");
    if(ack_queue.empty()){
        isNew = false;
        return 0;
    }
    //recv ack from queue
    size_t max_ack_seq = 0;
    while(!ack_queue.empty()){
        if(max_ack_seq < ack_queue.front())
        {
            max_ack_seq = ack_queue.front();
        };
        ack_queue.pop();
    }
    isNew = true;
    return max_ack_seq;
};

void FileTrannsferChannelSender::finish_transfer(size_t curr_seq)
{
    //after send all data, wait all acks of rest of packet
    //neet time out

    //check all ack
    bool new_ack;
    while(curr_seq != process_ack(new_ack)){
        msleep(10);
        printf("wait the last seq:%lu\n", curr_seq);
    };
    //finish packet, send more than once
    PacketMeta *pm = new PacketMeta(curr_seq, 'F');
    q_to_send.push(pm);
};

/*
 *
 * */
void* FileTrannsferChannelSender::reading_ack_thread(void *p)
{
    FileTrannsferChannelSender *pftc = (FileTrannsferChannelSender*)p;
    Packet *ack_pack;
    PacketMeta *pm = new PacketMeta();
    int test_ack = 0;
    while(true){
        //recv the ack from remote
        // recv call can be  blocked
        ack_pack = pftc->ack_receiver->receivePacket();
        pftc->parse_packet_header(ack_pack->accessHeader(), pm);
        pftc->ack_queue.push(pm->seq);
        printf("reading_ack_thread recv seq:%lu\n", pm->seq);
        /*pftc->ack_queue.push(test_ack); 
        printf("reading_ack_thread recv seq:%d\n", test_ack);
        test_ack += pftc->_default_payload_size;sleep(2);
        */
    };
    delete pm;
    return NULL;
};


void* FileTrannsferChannelSender::read_file_thread(void *p)
{
    FileTrannsferChannelSender *pftc = (FileTrannsferChannelSender*)p;
    //For test
    size_t to_read_size = 0;
    while(true)
    {
        while(true){
            to_read_size = pftc->_pbuf->avail_size();
            if(to_read_size > pftc->_default_payload_size * 2)
            {
                to_read_size -= pftc->_default_payload_size;
                break;
            }
            pftc->to_read_condv.Wait();
        }
        //already have buf to read file
        if( pftc->_pbuf->ends_to_top() < to_read_size ){//beyond the top 
            to_read_size = pftc->_pbuf->ends_to_top();
        };
        pftc->_is->read( pftc->_pbuf->contin_ends_point(), to_read_size);
        to_read_size = pftc->_is->gcount();
        //printf("read_file_thread read %lu bytes at:[%lu]\n", to_read_size, (size_t)pftc->_pbuf->contin_ends_point());
        pftc->_pbuf->move_contin_ends(to_read_size);
        if(to_read_size <= 0 && pftc->_is->eof()){
            //ret = -1;
            printf("read_file_thread reach the EOF\n");
            pftc->read_closed = true;
            break;
        }
    }
    return NULL;
};
void* FileTrannsferChannelSender::sending_packet_thread(void *p)
{
    FileTrannsferChannelSender *pftc = (FileTrannsferChannelSender*)p;
    printf("sending_packet_thread begin to send packets.....\n");
    Packet *pack = new Packet();
    while(true)
    {
        /* need infos:
         *  seq, data_point, data_len, type.... ===> PacketMeta
        *  send a packet based on the PacketMeta
        */
        PacketMeta* pm = pftc->q_to_send.pop();
        //print_pm(pm);
        pftc->fill_data_packet(pack, pm);
        //printf("sending_packet_thread begin to send packet seq:%lu, type:[%c]\n", pm->seq, pm->_type);
        //print_packet(pack);
        pftc->sender->sendPacket(pack);
        delete pm;// need opt
    }
    delete pack;
    return 0;
};

int SeqManager::setSeq(size_t seq){
    seq -= max_conti_seq;
    size_t index = seq / step_len;
    if(index*step_len != seq) return -1;
    index --;
    seq_list[index] = 1;
    int contin = 0;
    while( *(seq_list+contin) > 0){
        contin++;
    }
    if(contin <=0) return 0;
    int i;
    for(i=contin; i < list_size; i++){
        seq_list[i-contin] = seq_list[i];
    };
    max_conti_seq += ( step_len * contin);
    return 0;
};


void print_pm(PacketMeta* pm){
    printf("\nseq=[%lu], offset=[%d], dlen=[%d], pt=[%lu], type=[%c]\n\n",
            pm->seq,
            pm->offset,
            pm->data_len,
            (size_t)pm->_data_pt,
            pm->_type);
};

void print_packet(Packet *pk)
{
    size_t buf_size = 100;
    char* print_buf = new char[buf_size+1];
    if( pk->getPayloadSize() > buf_size){
        buf_size = pk->getPayloadSize() * 1.5;
        delete []print_buf;
        print_buf = new char[buf_size+1];
    }
    memset(print_buf, 0, buf_size);
    memcpy(print_buf, pk->getPayload(), pk->getPayloadSize());
    printf("\nPACKET >>>> [%s]\n\n", print_buf);
    delete []print_buf;
};


