
# Specify the compiler
CC = g++

CCOPTS = -std=c++11 -g #-Wall 

all: FtcSender FtcServer f

f: forwarding

forwarding : Queue.h forwarding.cpp forwarding.h
	$(CC) $(CCOPTS) forwarding.cpp -lpthread -o forwarding

common.o: common.h common.cpp
	$(CC) $(CCOPTS) -c common.cpp

conditionv.o: conditionv.cpp conditionv.h
	$(CC) $(CCOPTS) -c conditionv.cpp

ftc.o : common.o conditionv.o pack_buf.h Queue.h ftc.h ftc.cpp
	$(CC) $(CCOPTS) -c ftc.cpp

FtcSender:   ftc.o common.o conditionv.o ftc_client.cpp
	$(CC) $(CCOPTS) common.o conditionv.o ftc.o ftc_client.cpp -lpthread -o FtcSender 

FtcServer :  ftc.o common.o conditionv.o ftc_server.cpp ftc_server.h
	$(CC) $(CCOPTS) common.o conditionv.o ftc.o ftc_server.cpp -lpthread -o FtcServer 

clean :
	rm -f *.o FtcSender FtcServer forwarding

