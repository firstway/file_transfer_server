#include "common.h"
#include <iostream>
using namespace std;

class Packet;
class Port;
class Address;
class SendingPort;

int main(int argc, const char * argv[])
{

try {
  const char* hname = "localhost";       
  Address * my_addr = new Address(hname, 3000);
  Address * dst_addr =  new Address(argv[1], (short)(atoi(argv[2])));
  SendingPort *my_port = new SendingPort();
  my_port->setAddress(my_addr);
  my_port->setRemoteAddress(dst_addr);
  my_port->init();

  Packet * my_packet;
  for (int i=0; i< 30; i++)
    {
         my_packet = new Packet();
         my_packet->setPayloadSize(100);
         PacketHdr *hdr = my_packet->accessHeader();
	 hdr->setOctet('A',0);
	 hdr->setOctet('B',1);
	 hdr->setIntegerInfo(i,2);
	 //hdr->setSize(6);
	 my_port->sendPacket(my_packet); 
	 cout << "packet "<< i << " is sent!" <<endl;
         delete my_packet;
         sleep(1);
    }
} catch (const char *reason ) {
    cerr << "Exception:" << reason << endl;
    exit(-1);
  }  

 return 0;
}
