#include "Queue.h"
#include <iostream>
#include <unistd.h> //sleep

void* produce(void *pp) {
    Queue<int> *q = (Queue<int> *) pp;
  for (int i = 0; i< 10; ++i) {
    std::cout << "Pushing " << i << "\n";
    q->push(i);
    sleep(1);
  }
}

void* consume(void *pp) {
    Queue<int> *q = (Queue<int> *) pp;
  for (int i = 0; i< 25; ++i) {
    auto item = q->pop();
    std::cout << "Consumer "<< item << "\n";
  }
}

#include <thread>
int main()
{
  Queue<int> q;

    pthread_t thread1, thread2;

    pthread_create( &thread2, NULL, &consume, (void*)(&q));
    pthread_create( &thread1, NULL, &produce, (void*)(&q));

    pthread_join( thread1, NULL);
    pthread_join( thread2, NULL);

}

