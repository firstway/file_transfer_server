


#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "forwarding.h"

const size_t BUF_SIZE = 128*5;

const char* usage = "%s Port1-Remote1:RemotePort1  Port2-Remote2:RemotePort2";
/*
 * forwarding
 * -> Port1 -> Remote2:RemotePort2
 * -> Port2  -> Remote1:RemotePort1
 */

int parse_cmd(const char *s, int &listen_port, char *remote_add, int &remote_port)
{
    char tmp[20];
    char *p = strchr((char*)s,'-'); 
    if(NULL==p) return -1;
    memset(tmp, 0 , 20);
    memcpy(tmp, s, p-s);
    listen_port = atoi(tmp);
    
    p++;
    char *p2 = strchr(p,':');
    if(NULL==p) return -2;
    memcpy(remote_add, p, p2-p);
    remote_add[p2-p] = '\0';

    p2++;
    remote_port = atoi(p2);

    if(listen_port < 1024 || remote_port < 1024) return -3;
    return 0;
};

pthread_t create_recv_thread(Queue<char*> *q, int listen_port){
    thread_variable_recv *p_var_recv = new  thread_variable_recv();
    struct sockaddr_in myaddr;

    p_var_recv->scok_fd = socket(AF_INET, SOCK_DGRAM, 0);

    memset((char *)&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    myaddr.sin_port = htons(listen_port);

    if (bind(p_var_recv->scok_fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
        perror("bind failed");
            return 0;
    }

    p_var_recv->q = q;

    pthread_t pth;
    pthread_create( &pth, NULL, &port_recv_thread, (void*)(p_var_recv));
    return pth;
};

pthread_t create_send_thread(Queue<char*> *q, char *remote_add, int remote_port){
    thread_variable_send *p_var_send = new thread_variable_send();

    p_var_send->scok_fd = socket(AF_INET, SOCK_DGRAM, 0);

    struct sockaddr_in *servaddr = new struct sockaddr_in();    /* server address */
    /* fill in the server's address and data */
    memset((char*)servaddr, 0, sizeof(*servaddr));
    servaddr->sin_family = AF_INET;
    servaddr->sin_port = htons(remote_port);
    inet_pton(AF_INET, remote_add, &(servaddr->sin_addr));

    p_var_send->addrlen = sizeof(*servaddr);
    printf("sizeof servaddr:%d\n", p_var_send->addrlen);
    p_var_send->remote_addr = (struct sockaddr *)servaddr;

    p_var_send->q = q;

    pthread_t pth;
    pthread_create( &pth, NULL, &port_send_thread, (void*)(p_var_send));
    return pth;
};


#include <sys/select.h>
void msleep2(int ms)
{
    struct timeval delay;
    delay.tv_sec = 0;
    delay.tv_usec = ms * 1000; // ms
    select(0, NULL, NULL, NULL, &delay);
};

int main(int argc, const char * argv[]){
    /*
     * parse 
     * */
    if(argc < 3){
       printf(usage, argv[0]); 
    }; 

    int listen_port_1;
    char remote_addr_1[32];
    int remote_port_1;

    int listen_port_2;
    char remote_addr_2[32];
    int remote_port_2;

    parse_cmd(argv[1], listen_port_1, remote_addr_1, remote_port_1);
    parse_cmd(argv[2], listen_port_2, remote_addr_2, remote_port_2);
    printf("listen on [%d], remote [%s]:[%d]\n", listen_port_1, remote_addr_1, remote_port_1);
    printf("listen on [%d], remote [%s]:[%d]\n", listen_port_2, remote_addr_2, remote_port_2);
    //
    //
    struct sockaddr_in saddr;
    printf("sizeof saddr:%lu\n", sizeof(saddr));
    //

    Queue<char*> *q_prot1_to_remote2 = new Queue<char*>(); 
    Queue<char*> *q_prot2_to_remote1 = new Queue<char*>(); 

    pthread_t thread_recv_1 = create_recv_thread(q_prot1_to_remote2, listen_port_1);
    pthread_t thread_remote_2 = create_send_thread(q_prot1_to_remote2, remote_addr_2, remote_port_2);

    pthread_t thread_recv_2 = create_recv_thread(q_prot2_to_remote1, listen_port_2);
    pthread_t thread_remote_1 = create_send_thread(q_prot2_to_remote1, remote_addr_1, remote_port_1);

    while(true){
        msleep2(2000);
    };
    ///////////////////////
    return 0;
};


void* port_recv_thread(void *p){
    thread_variable_recv *var_recv = (thread_variable_recv*)p;
    printf("in recv_thread\n");
    char *buf;
    struct sockaddr_in remaddr;     /* remote address */
    socklen_t addrlen ;   

    while(true){
        buf = new char[BUF_SIZE+4];
        int recvlen = recvfrom(var_recv->scok_fd, buf+4, BUF_SIZE, 0, (struct sockaddr *)&remaddr, &addrlen);
        printf("received %d bytes\n", recvlen);
        if (recvlen > 0) {
            //buf[recvlen+4] = 0;
            //printf("received message: \"%s\"\n", buf+4);
        }
        memcpy(buf, (char*)&recvlen, 4);
        var_recv->q->push(buf);
    }
};


void* port_send_thread(void *p){
    thread_variable_send* var_send = (thread_variable_send*)p;
    printf("in send_thread\n");

    while(true){
        char *buf = var_send->q->pop();
        if(NULL == buf) break;
        int data_len;
        memcpy(&data_len, buf, 4);
        if(data_len <=0 || data_len > 1500){
            printf("Invalid data len:%d\n", data_len);
            delete buf;
            continue;
        };
        //printf("will send bytes %d\n", data_len);
        //buf[data_len+4] = '\0'; printf("send bytes[%s]\n", buf+4);
        int ret = sendto(var_send->scok_fd, buf+4, data_len, 0, (struct sockaddr *)(var_send->remote_addr), var_send->addrlen);
        delete buf;
        if(ret < 0){
            perror("sendto failed");
        }
    };
};

