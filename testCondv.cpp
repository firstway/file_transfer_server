


#include <stdio.h>
#include <stdlib.h>

#include <queue>          // std::queue
#include <unistd.h> //sleep
#include "conditionv.h"
using namespace std;


static queue<int> myqueue;


void* func1(void *p){
    CondV *pv = (CondV*)p;
    while(true){
        while(myqueue.empty()){
            pv->Wait();
        }
        printf("in thread 01\n");
        printf("rev:%d\n",myqueue.front());
        myqueue.pop();
    }

};

void* func2(void *p){
    CondV *pv = (CondV*)p;
    int i=9;
    while(true){
        printf("in thread 02\n");
        myqueue.push(i);
        pv->Signal();
        //printf("in thread 02 -------\n");
        i++;
        sleep(1);
        pv->Signal();
    };

};

void test_func_block(){
    queue<int> one_queue;
    one_queue.push(199);
    while(true){
        printf("block? ,get:%d\n", one_queue.front());
        one_queue.pop();
        sleep(1);
    }
}

int main()
{

    test_func_block();
    pthread_t thread1, thread2;

    CondV cv;
    pthread_create( &thread1, NULL, &func1, (void*)(&cv));
    pthread_create( &thread2, NULL, &func2, (void*)(&cv));

    pthread_join( thread1, NULL);
    pthread_join( thread2, NULL);

    printf("Final count: xxx\n");
    return 0;
}

