
Author： Biao Li (firstway ^#^ gmail^com OR biao.li93 ^#^ rutgers^edu)

Compile:
make ; # generate  FtcSender and FtcServer

Run:
1,  ./FtcServer 7700 8800
2,  ./FtcSender 127.0.0.1  7700  8800  /PATH/SOME-FILE
Then you can find a new file "SOME-FILE.COPY" in the same directory with FtcServer

Tested in gcc version 4.8.1 ( x86_64-linux-gnu )

Architecture:
Header :
	{version; # the version of protocol
	seq;       # seq of packet, the position of the packet in the whole stream in term of byte.
	Offset;  # for fragmentation
	data_len; # the size of the payload
	type;    #can be 'I'(init), 'A'(ACK), 'd'(data), 'F'(finish)
};

ACK:
Using fixed size window both in Server and  Sender.
Sender can send as many packets as  window size before  receives the first ack of these data.
Server can send ack selectively to the Sender. Ack cantains the #seq which means the ${seq} bytes have been all received and wants new data after ${seq};
Can deal with packet loss. For example, If Server  received packet 10, 11, 13, 14, 15. and packet 12 is lost. The #seq of ack should be 12. the Sender will send 12 as well as packet 16,17.....
Can deal with disorder packets. Because the #seq has indicates the position of the payload of the packet in the stream(file). So we can compute the continuous blocks as well as the missed blocks.

Concurrency:
using threads to deal with different I/Os; For example, in the Sender, one thread reads from the file, one thread sends the packet to remote and one thread receives the ACK packets, the main thread coordinates these threads and computes the max seq and decides to resend packets or not.

Performance:
Can transfer 3M file at the approximate rate of 2MB/s when using LossyReceivingPort
 and LossyReceivingPort. Both Server and  Sender are in the same machine.