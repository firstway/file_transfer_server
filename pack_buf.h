





#ifndef _FTC_PACKET_BUF_HEAD
#define _FTC_PACKET_BUF_HEAD



class PackBuf
{
public:
    PackBuf(){ _buf_size=0;};
    PackBuf(size_t bsize)
    {
        _buf_size = bsize;
        _buf = (char*)malloc(_buf_size);
        _data_size = 0;
        contin_starts = 0;
        contin_ends = 0;
    };

    size_t data_size(){
        return _data_size;
    };
    size_t avail_size(){
        return _buf_size - _data_size ;
    };

    int append(char* data, size_t dsize){
        if(dsize > avail_size()){
            return -1;
        }
        memcpy(_buf+contin_ends, data, dsize);
        contin_ends = (contin_ends+dsize) % _buf_size;
        _data_size += dsize;
        return 0;
    };

    int append(char* data, size_t dsize, size_t offset)
    {
        if(dsize+offset > avail_size()){
            return -1;
        }
        memcpy(_buf+contin_ends+offset, data, dsize);
        return 0;
    };

    int move_contin_starts(size_t distan)
    {
        if(distan > _data_size) return -1;
        contin_starts = (contin_starts+distan) % _buf_size;
        _data_size -= distan;
        return 0;
    };
    
    int move_contin_ends(size_t distan)
    {
        if( distan+_data_size > _buf_size) return -1;
        contin_ends = (contin_ends+distan) % _buf_size;
        _data_size += distan;
        return 0;
    };
    char* contin_starts_point(){
        return _buf + contin_starts;
    };
    char* contin_ends_point(){
        return _buf + contin_ends;
    };
    size_t buf_size(){return _buf_size;};

    size_t distance(char* up, char* down){
        return (_buf_size + up - down) % _buf_size;
    };

    size_t ends_to_top(){
        return _buf_size - contin_ends;
    };

    char* top_pt(){
        return _buf + _buf_size;
    };
    char* pt_by_seq(size_t seq){
        return (seq % _buf_size) + _buf;
    };
private:
    char *_buf;
    size_t _buf_size;
    size_t _data_size;
    size_t contin_starts;
    size_t contin_ends;
};




#endif



